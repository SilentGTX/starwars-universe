import axios from "axios";
import Entity from "./Entity";

class StarWarsUniverse {
  constructor() {
    this.entities = [];
  }

  async init() {
    const response = await fetch(`https://swapi.booost.bg/api/`);
    const data = await response.json();
    const keys = Object.keys(data);

    keys.forEach(async (id) => {
      const response2 = await fetch(`https://swapi.booost.bg/api/${id}`);
      const data2 = await response2.json();
      this.entities.push(new Entity(id, data2));
    });
  }
}

export default StarWarsUniverse;
